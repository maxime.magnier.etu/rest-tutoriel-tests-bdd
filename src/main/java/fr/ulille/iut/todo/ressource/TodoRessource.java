package fr.ulille.iut.todo.ressource;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import fr.ulille.iut.todo.service.TodoService;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

@Path("taches")
@Produces(MediaType.APPLICATION_JSON)
public class TodoRessource {
	private final static Logger LOGGER = Logger.getLogger(TodoRessource.class.getName());

	private TodoService todoService = new TodoService();

	@Context
	private UriInfo uri;
	@Context
	Request request;

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Tache> getAll() {
		LOGGER.info("getAll()");

		return todoService.getAll();
	}

	@GET
	@Path("{id}")
	public Tache getById(@PathParam("id") String id) {
		Tache t = todoService.getTache(UUID.fromString(id));
		if(t==null) throw new NotFoundException();
		return t;
	}

	@GET
	@Path("{id}/description")
	public String getDescription(@PathParam("id") String id) {
		Tache t = todoService.getTache(UUID.fromString(id));
		if(t==null) throw new NotFoundException();
		String description = t.getDescription();
		return description;
	}

	@POST
	public Response createTache(CreationTacheDTO tacheDto) {
		LOGGER.info("createTache()");

		Tache tache = Tache.fromCreationTacheDTO(tacheDto);
		todoService.addTache(tache);
		URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

		EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
		ResponseBuilder builder = request.evaluatePreconditions(etag);

		if (builder == null) {
			builder = Response.created(location);
			builder.tag(etag);
			builder.entity(tache);
		}
		return builder.build();
	}

	@POST
	public Response createTacheFromForm(MultivaluedMap<String, String> formParams) {
		Tache tache = new Tache();
		tache.setNom(formParams.getFirst("nom"));    
		tache.setDescription(formParams.getFirst("description"));
		
		todoService.addTache(tache);
		URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

		EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
		ResponseBuilder builder = request.evaluatePreconditions(etag);

		if (builder == null) {
			builder = Response.created(location);
			builder.tag(etag);
			builder.entity(tache);
		}
		return builder.build();
	}
	
	@DELETE
	@Path("{id}")
	public void delete(@PathParam("id") String id) {
		Tache t = todoService.getTache(UUID.fromString(id));
		if(t==null) throw new NotFoundException();
		todoService.deleteTache(id);
	}
	
	@PUT
	@Path("{id}")
	public Tache update(@PathParam("id") String id, Tache tache) {
		Tache t = todoService.getTache(UUID.fromString(id));
		if(t==null) throw new NotFoundException();
		
		if(tache.getNom() == null || tache.getNom().equals("") || tache.getDescription() == null || tache.getDescription().equals("")) {
			throw new BadRequestException();
		}
		
		todoService.updateTache(tache);
		return t;
	}
}
